using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Bola : MonoBehaviour
{
    // Velocidad de movimiento - D
    public float speed = 10f;

    EnemieController enemie;

    [SerializeField] private Rigidbody2D ballRb; 
    [SerializeField] private float velocityMult;
    [SerializeField] private float velocityEnem;


    [SerializeField] public float puntos;
    [SerializeField] public float cantidadpuntos;
    public TextMeshProUGUI puntaje_;
   
    

    // Le decimos hacia que direccion se va a empezar a mover la pelota, en este caso hacia arriba - D
    private void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
    }

    public void Update()
    {
        if (GetComponent<Bola>().enabled != false)
        {
            transform.parent = null;
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Sabemos si se colisiona o no con el jugador - D
        if (collision.gameObject.CompareTag("Player"))
        {
            // Calculamos el valor para saber en que posicion golpea a la pelota y darle una direccion distinta - D
            float x = HitFactor(transform.position,
                                collision.transform.position,
                                collision.collider.bounds.size.x);
           
            // Normalizamos la direccion - D
            Vector2 dir = new Vector2(x, 1).normalized;

            // Calculamos la velocidad multiplicando la direccion y la velocidad que le asignemos - D
            GetComponent<Rigidbody2D>().velocity = dir * speed;
        }

        if (collision.gameObject.CompareTag("Brick"))
        {
            puntos += 20;
            puntaje_.text = puntos.ToString("0");
            Destroy(collision.gameObject);
            ballRb.velocity *= velocityMult;
            Debug.Log(puntos);
            if (puntos >= cantidadpuntos)
            {
                SceneManager.LoadScene("Win");
            }
            
        }
    }

    float HitFactor(Vector2 ballpos, Vector2 playerPos, float playerWidth)
    {
        // Sabemos en que punto colisiona la posicion de la pelota con la posiscion del jugador para asi determinar el movimiento - D
        return (ballpos.x - playerPos.x) / playerWidth;
    }


}
