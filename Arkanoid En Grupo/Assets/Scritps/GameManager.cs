using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public int livesCount = 3;
    public TextMeshProUGUI livesText;

    private void Start()
    {
        livesText.text = ":" + livesCount.ToString();
    }

    public void LoseLives()
    {
        livesCount--;
        livesText.text = ":" + livesCount.ToString();

        if (livesCount <= 0)
        {
            Debug.Log("Se acabaron las vidas");
            SceneManager.LoadScene("Game Over");
        }
    }

    // Zona de collision y muerte - D
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == CompareTag("Death_Zone"))
        {
            if (collision.gameObject.tag == "Ball")
            {
                Debug.Log("Murio");
                FindObjectOfType<Movimiento>().PowerUp3();
                FindObjectOfType<GameManager>().LoseLives();
            }
        }
    }






}
