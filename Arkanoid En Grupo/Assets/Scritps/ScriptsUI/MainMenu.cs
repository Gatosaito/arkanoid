using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //todos los public de este scrpit hacen referencia a cada uno de los niveles del juego para llamarlos desde los botones
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Nivel1()
    {
        SceneManager.LoadScene("Level 1");
    }
    public void Nivel2()
    {
        SceneManager.LoadScene("Level 2");
    }
    public void Nivel3()
    {
        SceneManager.LoadScene("Level 3");
    }
    public void Nivel4()
    {
        SceneManager.LoadScene("Level 4");
    }
    public void Nivel5()
    {
        SceneManager.LoadScene("Level 5");
    }
}
