using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieController : MonoBehaviour
{
    public float speed = 10f;

    public int vidas; 

    [SerializeField] private Rigidbody2D ballRb;
    [SerializeField] private float velocityMult;

    private void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
    }
    public void EnemieLife(Collision2D collision)
    {
       
        if (collision.gameObject.CompareTag("Ball"))
        {
            vidas--;
            if (vidas <= 0)
            {
                Destroy(gameObject);
            }
        }
    }


}
