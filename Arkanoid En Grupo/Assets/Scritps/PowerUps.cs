using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    [SerializeField] public Transform xRangeLeft;
    [SerializeField] public Transform xRangeRight;
    [SerializeField] public Transform yRangeLeft;
    [SerializeField] public Transform yRangeRight;

    public GameObject[] powerUps;

    public float spawnTime = 1;

    public float repeatSpawnRate = 3;

    private void Start()
    {
        InvokeRepeating("SpawnPowerUps", spawnTime, repeatSpawnRate);
    }

    public void SpawnPowerUps()
    {
        Vector3 spawnPosition = new Vector3(0, 0, 0);

        spawnPosition = new Vector3(Random.Range(xRangeLeft.position.x, xRangeRight.position.x), Random.Range(yRangeRight.position.y, yRangeLeft.position.y), 0);

        GameObject powerUps = Instantiate(this.powerUps[Random.Range(0, this.powerUps.Length)], spawnPosition, gameObject.transform.rotation);
    }
}
