using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    // Velocidad de movimiento para el jugador - D
    public float speed = 150;

    [SerializeField] Transform ballPos;
    [SerializeField] Transform playerPos;

    public GameObject bola;

    TipoPowerUp powers;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PowerUp"))
        {
            if(collision.GetComponent<TipoPowerUp>() != null)
            {
                powers = collision.GetComponent<TipoPowerUp>();
                if (powers.pUp1 == true)
                {
                    PowerUp1();
                    Debug.Log("Power Up 1");
                    Destroy(collision.gameObject);
                    powers = null;
                }
                if (powers.pUp2 == true)
                {
                    PowerUp2();
                    Debug.Log("Power Up 2");
                    Destroy(collision.gameObject);
                    powers = null;
                }

                if (powers.pUp3 == true)
                {
                    PowerUp3();
                    Debug.Log("Power Up 3");
                    Destroy(collision.gameObject);
                    powers = null;
                   
                }
            }
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            FindObjectOfType<Bola>().enabled = true;
        }
    }

    private void FixedUpdate()
    {
        // Obtener el Input del teclado, devuelve valores entre -1 y 1 (Izquierda o Derecha) - D
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        // Darle movimiento a la barra mediante su componente transform y hacer los calculos para que se mueva - D
        transform.position += new Vector3(horizontalInput * speed * Time.deltaTime ,0f);

        // Otra posible opcion de movimiento que funcionaria por fisicas - D
        //GetComponent<Rigidbody2D>().velocity = Vector2.right * horizontalInput * speed * Time.deltaTime;

    }

    public void PowerUp1()
    {
        FindObjectOfType<Bola>().speed = 17;
    }
    public void PowerUp2() 
    {
        speed = 20;
    }

    public void PowerUp3() 
    {
        ballPos.transform.position = playerPos.transform.position;
    }
}
